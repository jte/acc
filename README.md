ACC
===

This fork of the ACC utility used to compile ACS scripts for ZDoom and its derivatives is intended to provide the following functionality:
Overriding line specials with script functions.

This is due to the fact that not all ZDoom derivatives posses all the same internal functions, most notably some basic math functions added in recent versions is still missing from Zandronum, and it would be easy to just recreate the missing functionality by writing a simple user script function which performs the same operation until the engine in question is updated to support them.

Without this ACC modification, overriding line specials would be impossible and either I, personally, would need to keep multiple copies of zspecial.acs to account for working with multiple versions of the engine, or otherwise would have to rename the function even though it does the same goddamn thing.